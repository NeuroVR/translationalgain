Quick and dirty test to create some translational gain by changing the scale of the environment and the VR camera's eye height.
In this example the gain can be toggled between normal movement speed and slowed down movement by 50% by pressing the right or left trigger button (currently only for Vive controllers)
